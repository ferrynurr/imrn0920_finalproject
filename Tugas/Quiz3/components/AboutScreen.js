
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
  TextInput,
  Button
} from 'react-native';

import { AuthContext } from "../context";
import { AntDesign } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons';   
import { Feather } from '@expo/vector-icons'; 

const AboutScreen = () => {
 const { signOut } = React.useContext(AuthContext);
    return (
    <View style={styles.container}>   
       
        <Text style={{fontSize:28, fontWeight: 'bold', color: '#003366', paddingBottom:25}}>Tentang Saya</Text>
        <FontAwesome name="user-circle" size={130} color="#3EC6FF" />
        <Text style={{fontSize:20, fontWeight: 'bold', color: '#003366', paddingTop:10}}>M. Ferry Nurdin</Text>
        <Text style={{fontSize:15, fontWeight: 'bold', color: '#3EC6FF'}}>Web Developer</Text>
        {/* <Text style={{fontSize: 18, color: '#003366'}} onPress={() => signOut()}>Logout</Text> */}
          <TouchableHighlight
                    style={styles.submit}

                    underlayColor='#fff'>
                    <Text style={[styles.submitText, styles.btnDaftar]} onPress={() => signOut()} >Logout <Entypo name="log-out" size={18} color="white" /> </Text>
                </TouchableHighlight>
        
            <View style={[styles.col, {flex:0.6}]} >
            <Text style={{ fontWeight: 'bold', color: '#003366'}}>Akun Media Sosial</Text>
                <View style={{width: '100%', height: 2, backgroundColor: '#003366', marginBottom:10}}></View>  
                    <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{width: '50%', alignItems: 'center'}}>
                            <Feather name="globe" size={60} color="#3EC6FF" />
                            <Text style={{paddingTop:4, color: '#003366'}} >ewistudio.my.id</Text>
                    </View>
                    <View style={{width: '50%', alignItems: 'center'}} >
                        <AntDesign name="github" size={60} color="#3EC6FF" />
                        <Text style={{paddingTop:4, color: '#003366'}} >@ferrynurr</Text>
                    </View>
                </View> 
        </View>


       

    </View>
    );
  
}
export default AboutScreen;

const styles = StyleSheet.create({
  container: {
        flex: 1,
        flexDirection: 'column',
        paddingTop:60,
        alignItems: 'center',
        margin:19,
  },

  col: {
    
    backgroundColor: "grey",
    borderWidth: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,

    paddingTop: 10,
 
    borderColor: '#EFEFEF',
    backgroundColor: '#EFEFEF',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    width: '100%',
    marginTop:21,
  
  },

  submitText:{
    color:'#fff',
    textAlign:'center',
    fontWeight: 'normal',
    fontSize: 17,
    marginTop:12
  },

  btnDaftar : {
    padding:8,
    borderRadius:20,
    backgroundColor:'red'
  },

});