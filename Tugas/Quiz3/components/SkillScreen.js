import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StatusBar,
  TextInput,
  ScrollView,
  ActivityIndicator,
  Button
} from 'react-native';



import { Entypo } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons';   
import flashsale from '../flashsale.json';
import produk from '../produk.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Ionicons } from '@expo/vector-icons'; 
import { FontAwesome5 } from '@expo/vector-icons'; 
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { EvilIcons } from '@expo/vector-icons'; 

const SkillScreen = () => {
 const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://sigpukan.net/FerryStore?limit=10')
      .then((response) => response.json())
      .then((json) => setData(json.list_produk))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);




    return (
    
    <View style={styles.container}> 

        <View style={styles.header}>
          <Text style={styles.headerText}></Text>
        </View> 
         <View style={styles.content}>
              <View style={styles.tentang}>
                    <View style={{width: '85%', height: 50, }}>
                        <TextInput placeholder="Cari sesuatu..."
                            style={ styles.inputText }
                        />
                    </View>
                    <View style={{width: '15%', height: 50, justifyContent: 'center', alignItems: 'center' }} >
                       <Ionicons name="ios-notifications" size={24} color="#F77866" />
                         
                    </View>
                    
              </View>
               <View style={styles.logo}>
                <Image source={require('../images/banner.jpg')} style={{ width: '100%', height: 200 }} />
              </View>
                <View style={styles.tentang}>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                             <MaterialCommunityIcons name="food-apple" size={24} color="white" />
                            
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Buah</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                           <MaterialCommunityIcons name="flower-outline" size={24} color="white" />
                            
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Bunga</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                            <MaterialCommunityIcons name="tree" size={24} color="white" />

                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Sayur</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={styles.katagori}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                           <FontAwesome name="tree" size={24} color="white" />
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>Pohon Hias</Text>
                    </View>
                    <View style={{width: '20%', justifyContent: 'center'}}>
                       <View style={[styles.katagori, { backgroundColor: 'black'}]}>
                          <Text style={{fontSize:12, fontWeight: 'bold'}}>
                           <Entypo name="chevron-thin-right" size={24} color="white" />
                          </Text> 
                        </View>
                         <Text style={{alignSelf: 'center'}}>More</Text>
                    </View>

                </View>

        <Text style={{marginTop: 20, fontWeight: 'bold', fontSize:17}}>List Produk</Text>
           <FlatList
            horizontal={true}
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
                <TouchableOpacity  style={styles.item}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{width: 60, height: 50,}}>
                          <MaterialCommunityIcons name="food-apple" size={60} color="black" />
                      </View>
                      <View style={{width: 140, height: 50}}>
                          <Text style={styles.skillName}>{item.produk}</Text>
                          <Text style={styles.percentageProgress}>Rp.{item.harga}</Text>
                      </View>
                     <View style={{width: 50, height: 50, marginTop:15, justifyContent: 'center'}}>
                      <Text  > <EvilIcons name="arrow-right" size={50} color="white" /> </Text>
                    </View>
                    </View>
                   
                </TouchableOpacity>
            )}
          />

         </View> 

    </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
         backgroundColor: '#fff',
  },

  col: {
    
    backgroundColor: "grey",
    borderWidth: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,

    paddingTop: 10,
 
    borderColor: '#EFEFEF',
    backgroundColor: '#EFEFEF',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    width: '100%',
    marginTop:21,
  
  },
    logo : {
    flexDirection: 'column',
    alignItems: 'flex-end',
    marginTop:20
  },
  header: {
    backgroundColor: '#F77866',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0,
   borderBottomColor: '#ddd',
  },
  headerText: {
    padding: 8,
  },

  content: {
    margin:15
  },
  tentang: {
        flexDirection: 'row',
        marginTop:20
  },
  skillHead:{
    marginTop:30
  },
  border:{
     width: '100%', 
     height: 5, 
     backgroundColor: '#003366',
     marginBottom:10
  },
  katagori: {
       height: 50, 
       backgroundColor: '#F77866',   
       borderBottomLeftRadius: 10,
       borderBottomRightRadius: 10,
       borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      marginRight:5,
      justifyContent: 'center',
      alignItems: 'center',
      
     
  },
  container2: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#B4E9FF',
    padding:8,
    marginVertical: 8,
    marginHorizontal: 8,
    height:100
  },
  title: {
    fontSize: 32,
  },
  skillName: {
      fontSize:20,
      fontWeight:'bold',
  },
  categoryName: {
      fontSize:15,
      fontWeight:'bold',
      color: '#19d3da',
      
  },
  percentageProgress: {
      fontSize:35,
      fontWeight:'bold',
      color: '#FFF',
      //alignSelf: "flex-end"
  },
  inputText: {
    borderBottomWidth:2,
    width: '100%',
    height: 40,
    marginBottom: 20,
    borderBottomColor: '#F77866',
    color: 'black',
    fontSize: 20,
    paddingLeft: 8,
    paddingRight: 8
  },
});

export default SkillScreen;